package com.heist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:spring.xml")
public class HeistApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeistApplication.class, args);
	}
}
