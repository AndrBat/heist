package com.heist.server.controller;


import com.google.gson.Gson;
import com.heist.server.model.AttackResults;
import com.heist.server.model.Bank;
import com.heist.server.model.Heist;
import com.heist.server.model.Thief;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class HeistController {

    private Bank bank;
    private Thief thief;
    private Gson gson = new Gson();

    @Autowired
    HeistController(Bank bank, Thief thief) {
        this.bank = bank;
        this.thief = thief;
    }

    /**
     * To call this mapping Run application and follow the URL http://localhost:8080/start?name=Dimas
     * OR
     * Open terminal and type:
     * curl "http://localhost:8080/start?name=DimasTerminalMaster"
     *
     * @param name input your Name to create a Thief.
     * @return new serialized Heist game instance
     */
    @GetMapping(value = "/start", produces = MediaType.APPLICATION_JSON_VALUE)
    public String startGame(@RequestParam String name) {
        //Here we create new game
        Heist newGame = createNewGame(name);
        //Here we serialize game into a String representation.
        return gson.toJson(newGame);
    }

    /**
     * Open terminal and type:
     * curl -v -H "Content-Type: application/json" POST -d 'INPUT_HERE_RESPONSE_FROM_START_OR_ATTACK_ENDPOINT' http://localhost:8080/attack
     *
     * Example.
     * curl -v -H "Content-Type: application/json" POST -d '{"bank":{"heading":"BEGEMOT","deposit":100000,"security":0,"vault":0},"thief":{"alias":"Dimas","cash":1000,"expierense":1,"physics":5},"numberOfRobberies":0,"attackResult":{"message":"NO BANK HEISTS SO FAR!","stolenCash":0}}' http://localhost:8080/attack
     *
     * Each time to modify state copy response and call this endpoint again with new state
     *
     * @param gameInstance previous state of the game
     * @return new state of the game
     */
    @PostMapping(value = "/attack", produces = MediaType.APPLICATION_JSON_VALUE)
    public String attackBank(@RequestBody String gameInstance) {
        Heist heist = gson.fromJson(gameInstance, Heist.class);
        Heist modifiedGame = attack(heist);
        return gson.toJson(modifiedGame);
    }



    //TODO Move out of HERE
    private Heist attack(Heist heist) {
        Thief thief = heist.getThief();
        Bank bank = heist.getBank();

        if ((thief.getExpierense() > bank.getVault()) || (thief.getPhysics() >= bank.getSecurity())) {
            thief.setCash(bank.getDeposit() / (10 - thief.getExpierense()));
            heist.setAttackResult(generateAttackResult("CONGRATULATIONS! YOU JUST ROBBED A BANK"));
            heist.incrementNumberOfRobberies();
        } else if ((thief.getExpierense() < bank.getVault()) && (thief.getPhysics() < bank.getSecurity())) {
            bank.setDeposit(bank.getDeposit() + thief.getCash());
            heist.setAttackResult(generateAttackResult("OH NO! THIS BASTARDS GRABBED YOU, THEY TOOK ALL YOUR MONEY"));
        } else {
            heist.setAttackResult(generateAttackResult("OH , THEY CHANGED LOCKS, LETS TRY NEXT TIME < WE HAVE TO ESCAPE!"));
        }
        return heist;
    }

    //TODO Move out of HERE
    private AttackResults generateAttackResult(String message) {
        return new AttackResults(message, thief.getCash(), bank.getHeading());
    }

    //TODO Move out of HERE
    private Heist createNewGame(String name) {
        thief.setAlias(name);
        return new Heist(bank, thief);
    }

}
