package com.heist.server.model;

public class AttackResults {

    private String message;
    private int stolenCash;
    private String bankName;

    public AttackResults(String message, int cash, String heading) {
        this.message = message;
        this.stolenCash = cash;
        this.bankName = heading;
    }

    public AttackResults(String message, int stolenCash) {
        this.message = message;
        this.stolenCash = stolenCash;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStolenCash() {
        return stolenCash;
    }

    public void setStolenCash(int stolenCash) {
        this.stolenCash = stolenCash;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}

