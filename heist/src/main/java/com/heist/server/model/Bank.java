package com.heist.server.model;

import java.util.Random;

public class Bank {
    private String heading;
    private int deposit;
    private int security;
    private int vault;

    public Bank(String heading, int deposit, int security, int vault) {
        this.heading = heading;
        this.deposit = deposit;
        this.security = security;
        this.vault = vault;
    }

    public Bank(String heading, int deposit) {
        Random random = new Random();
        this.heading = heading;
        this.deposit = deposit;
        this.security =  random.nextInt() * 10;
        this.vault =  random.nextInt() * 10;
    }

    public Bank() {

    }

    public int getVault() {
        return vault;
    }

    public void setVault(int vault) {
        this.vault = vault;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getSecurity() {
        return security;
    }

    public void setSecurity(int security) {
        this.security = security;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "heading='" + heading + '\'' +
                ", deposit=" + deposit +
                ", security=" + security +
                ", vault=" + vault +
                '}';
    }
}
