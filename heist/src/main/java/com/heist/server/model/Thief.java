package com.heist.server.model;

public class Thief {

    private String alias;
    private int cash;
    private int expierense = (int) (Math.random()*10);
    private int physics = (int) (Math.random()*10);

    public Thief(String alias, int cash, int expierense, int physics) {
        this.alias = alias;
        this.cash = cash;
        this.expierense = expierense;
        this.physics = physics;
    }

    public Thief() {

    }

    public int getPhysics() {
        return physics;
    }

    public void setPhysics(int physics) {
        this.physics = physics;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getExpierense() {
        return expierense;
    }

    public void setExpierense(int expierense) {
        this.expierense = expierense;
    }

    @Override
    public String toString() {
        return "Thief{" +
                "alias='" + alias + '\'' +
                ", cash=" + cash +
                ", expierense=" + expierense +
                ", physics=" + physics +
                '}';
    }
}
