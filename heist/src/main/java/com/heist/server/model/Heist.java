package com.heist.server.model;

import org.springframework.beans.factory.annotation.Autowired;

public class Heist {

    private Bank bank;
    private Thief thief;
    private int numberOfRobberies;
    private AttackResults attackResult;


    @Autowired
    public Heist(Bank bank, Thief thief) {
        this.bank = bank;
        this.thief = thief;
        this.attackResult = new AttackResults("NO BANK HEISTS SO FAR!", 0);
        this.numberOfRobberies = 0;
    }

    public void incrementNumberOfRobberies() {
        this.numberOfRobberies++;
    }

    public int getNumberOfRobberies() {
        return numberOfRobberies;
    }

    public void setNumberOfRobberies(int numberOfRobberies) {
        this.numberOfRobberies = numberOfRobberies;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Thief getThief() {
        return thief;
    }

    public void setThief(Thief thief) {
        this.thief = thief;
    }

    public AttackResults getAttackResult() {
        return attackResult;
    }

    public void setAttackResult(AttackResults attackResult) {
        this.attackResult = attackResult;
    }
}





